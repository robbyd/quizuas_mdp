package edu.stts.QuizMDP6551;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class GameActivity extends AppCompatActivity {
    String level;
    int life = 5;
    int angka1, angka2;
    private appDatabase db;
    Button btnGo, btnGive;
    TextView tvLife, tvLevel, tvAngka1, tvAngka2, tvAngka3;
    EditText etJawab;
    String username;

    Random rnd = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        if (getIntent()!= null){
            username = getIntent().getStringExtra("username");
        }
        tvLife = findViewById(R.id.tvLife);
        tvLevel = findViewById(R.id.tvLevel);
        tvAngka1 = findViewById(R.id.angka1);
        tvAngka2 = findViewById(R.id.angka2);
        tvAngka3 = findViewById(R.id.angka3);
        etJawab = findViewById(R.id.etJawab);
        db = Room.databaseBuilder(getApplicationContext(), appDatabase.class,
                "dbscore").build();

        btnGo = findViewById(R.id.btGo);
        btnGive = findViewById(R.id.btGive);

        if(getIntent()!= null){
            level = getIntent().getStringExtra("level");
        }
        tvLevel.setText("Level : " + level);
        if(level.equals("Easy")){
            angka1 = rnd.nextInt(10);
            angka2 = rnd.nextInt(10);
        }else if(level.equals("Medium")){
            angka1 = rnd.nextInt(100);
            angka2 = rnd.nextInt(100);
        }else if(level.equals("Hard")){
            angka1 = rnd.nextInt(1000);
            angka2 = rnd.nextInt(1000);
        }

        tvAngka1.setText(angka1+"");
        tvAngka2.setText(angka2+"");
    }
    private void insertDataScore(final Score b){
        new AsyncTask<Void, Void, Long>(){
            @Override
            protected Long doInBackground(Void... voids) {
                long status = db.scoreDao().insertScore(b);
                return status;
            }
        }.execute();
    }
    public void goClick(View v){
        if(btnGo.getText().toString().equals("DONE")){
            String hasil = "You Win ! \n Score : " + (life*20);
            insertDataScore(new Score(username, level, life*20));
            Toast.makeText(getApplicationContext(), hasil, Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("username", username);
            startActivity(i);
        }else{
            if(!etJawab.getText().toString().equals("")){
                int angka3 = Integer.parseInt(etJawab.getText().toString());
                if(angka3 == (angka1+angka2)){
                    tvAngka3.setText(etJawab.getText());
                    Toast.makeText(getApplicationContext(), "You're Right", Toast.LENGTH_LONG).show();
                    btnGo.setText("DONE");
                }else{
                    life--;
                    tvLife.setText("Life : " + life);
                    Toast.makeText(getApplicationContext(), "You're Wrong", Toast.LENGTH_LONG).show();
                    if(life==0){
                        Toast.makeText(getApplicationContext(), "Game Over", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(this, MainActivity.class);
                        i.putExtra("username", username);
                        startActivity(i);
                    }
                }
            }else{
                Toast.makeText(getApplicationContext(), "Field cannot be empty!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void giveClick(View v){
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("username", username);
        startActivity(i);
    }
}
