package edu.stts.QuizMDP6551;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity
                        implements LevelFragment.OnFragmentInteractionListener,
                                    ScoreFragment.OnFragmentInteractionListener{

    FragmentTransaction ft;
    Button btPlay;
    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.mainFragment, new LevelFragment());
        ft.commit();
        if (getIntent() != null){
            username = getIntent().getStringExtra("username");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menuLevel){
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFragment, new LevelFragment());
            ft.commit();
        }else if(item.getItemId() == R.id.menuScore){
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFragment, new ScoreFragment());
            ft.commit();
        }else if(item.getItemId() == R.id.menuOut){
            startActivity(new Intent(this, LoginActivity.class));
        } return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentInteraction(String level) {
        Intent i = new Intent(this, GameActivity.class);
        i.putExtra("level", level);
        i.putExtra("username", username);
        startActivity(i);
    }
}
