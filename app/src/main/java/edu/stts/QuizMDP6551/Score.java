package edu.stts.QuizMDP6551;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import java.io.Serializable;
@Entity(tableName = "tbScore")
public class Score implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int idScore;
    @ColumnInfo(name = "Username")
    private String username;
    @ColumnInfo(name = "Level")
    private String level;
    @ColumnInfo(name = "Score")
    private int score;
    public Score(String username, String level, int score) {
        this.username = username;
        this.level = level;
        this.score = score;
    }

    public int getIdScore() {
        return idScore;
    }

    public void setIdScore(int idScore) {
        this.idScore = idScore;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return level + " - " + username + " - " + score;
    }
}
