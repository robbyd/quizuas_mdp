package edu.stts.QuizMDP6551;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    EditText etUsername, etPassword;
    RequestQueue rq;
    String url = "http://simpleservicesoa2018.000webhostapp.com/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        rq = Volley.newRequestQueue(this);
    }

    public void loginClick(View v){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject resp = new JSONObject(response.toString());
                        Toast.makeText(getApplicationContext(), resp.getString("message"), Toast.LENGTH_LONG).show();
                        if(resp.getInt("code") == 1){
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            i.putExtra("username", etUsername.getText().toString());
                            startActivity(i);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String username;
                if (etUsername.getText().toString().equals("") && etPassword.getText().toString().equals("")){
                    username = "Invalid";
                }else{
                    username = etUsername.getText().toString();
                }
                Map<String,String> parameters = new HashMap<String,String>();
                parameters.put("username", username);
                parameters.put("password", etPassword.getText().toString());
                return parameters;
            }
        };
        rq.add(stringRequest);
    }
}
