package edu.stts.QuizMDP6551;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
@Dao
public interface ScoreDAO{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertScore(Score score);
    @Update
    int updateScore(Score score);
    @Delete
    int deleteScore(Score score);
    @Query("SELECT * FROM tbScore ORDER BY score DESC")
    Score[] selectAllScores();
    @Query("SELECT * FROM tbScore where idScore=:id")
    Score selectScoreDetail(int id);
}
