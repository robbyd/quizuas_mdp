package edu.stts.QuizMDP6551;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
@Database(entities = {Score.class}, version = 1)
public abstract class appDatabase extends RoomDatabase {
    public abstract ScoreDAO scoreDao();
}
